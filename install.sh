#!/bin/bash

# Stow everything
stow -t "$HOME" -v  X11 i3 vim zsh alacritty ssh git polybar deadd betterlockscreen cursor_theme pulse npm picom user-dirs gtk awesome python espanso qt5ct

# Default to qwerty for i3
i3/.config/i3/create.sh
ln -sfv "$HOME"/.config/i3/config-qwerty "$HOME"/.config/i3/config

# Create the necessary user directories
mkdir "$HOME"/documents "$HOME"/downloads "$HOME"/pictures "$HOME"/desktop
