# Spot-Base

## AUR Packages
A base configuration for all my systems.

The necessary AUR packages can be intalled with

```bash
yay --aur -S --asdeps awesome-git autojump betterlockscreen xidlehook tldr++ find-the-command paccache-hook
```

`informant` is special because it will block the rest of the install.
Install it by itself first

```bash
yay --aur -S --asdeps informant
# Add yourself to the informant group
sudo usermod -aG informant $USER
# Generate the databases
sudo pacman -Fy
# log off and then log back in
pkill -KILL -u $USER
```

## Post-Install

This might be automated later, for now:

```bash
# Enable SDDM
sudo systemctl enable --now sddm
```

## TODOs

Add configuration files for:
    
 - [ ]  SDDM
 - [ ] zshenv
