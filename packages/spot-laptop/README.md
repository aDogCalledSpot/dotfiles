# Spot-Laptop

A base configuration for my laptop.

## AUR Packages

There are no AUR packages in this meta-package

## Post-Install

There are no post-install instructions but make sure to use the microcode correctly and edit and copy the file at `/usr/share/X11/xorg.conf.d/70-synaptics.conf`.
