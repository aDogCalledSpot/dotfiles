# Spot-Base

## AUR Packages
A base configuration for all my systems.

The necessary AUR packages can be intalled with

```bash
yay --aur -S --asdeps appimagelauncher spotify-adblock whatsapp-nativefier
```

## Post Install

There are no post-install instructions
