# Spot-Desktop

A base configuration for my desktop.

## AUR Packages


```bash
yay --aur -S --asdeps brscan4 brother-dcp1610w firefox-nightly cproton-git gamemode
```

## Post-Install

There are no post-install instructions but make sure to use the microcode correctly.