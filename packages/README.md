# Meta-Packages

Each folder has its own meta-package.

Not all dependencies can be resolved by `makepkg` because some packages are in the AUR.
These are listed at the top of the dependency list of each meta-package for this purpose.

You can install these packages with

```
yay --aur -S --asdeps <packages>
```

then install the rest with

```
# This should happen in the folder of the meta-package you want to install
makepkg -si
```

There may be post-install instructions along with each package.
Check the READMEs in each folder.
