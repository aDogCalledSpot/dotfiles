# Spot-Desktop

A base configuration for my desktop.

## AUR Packages

No AUR packages

## Post-Install

```bash
# Set up UFW rules
sudo ufw limit 22/tcp  
sudo ufw allow 80/tcp  
sudo ufw allow 443/tcp  
# KDE Connect
sudo ufw allow 1714:1764/udp
sudo ufw allow 1714:1764/tcp


sudo ufw default deny incoming  
sudo ufw default allow outgoing
sudo ufw enable


# Enable fail2ban
sudo systemctl enable fail2ban
sudo systemctl start fail2ban
```
