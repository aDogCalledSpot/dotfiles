# Set zsh plugins
autoload -Uz compinit
setopt autocd extendedglob nomatch

# Find-the-command
source /usr/share/doc/find-the-command/ftc.zsh noprompt quiet

# Powerlevel10k
source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
source $ZDOTDIR/p10k.conf

# Autojump
source /usr/share/autojump/autojump.zsh

# fzf
source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

source $ZDOTDIR/aliases
source $ZDOTDIR/private_aliases
source $ZDOTDIR/special_keys.zsh

# Ignore ugly highlighting of 777 dirs
LS_COLORS=$LS_COLORS:'ow=1;34:' ; export LS_COLORS

compinit
