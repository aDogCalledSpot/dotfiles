-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

local globals = require("config.global_vars")
-- Themes define colours, icons, font and wallpapers.
local beautiful = require("beautiful")
beautiful.init(globals.get_theme_dir() .. "theme.lua")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
local naughty = require("naughty")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
-- Notification library
local menubar = require("menubar")
local widgets = require("themes.spot.widgets")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- Error handling
local error_handling = require("config.errors")
error_handling.check_startup()
error_handling.check_runtime()

-- Startup
awful.spawn.with_shell(gears.filesystem.get_configuration_dir() .. "scripts/autorun.sh")



require("themes.spot.widgets.exit_screen")

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    awful.layout.suit.tile,
    -- awful.layout.suit.tile.left,
    -- awful.layout.suit.tile.bottom,
    -- awful.layout.suit.tile.top,
    awful.layout.suit.floating,
    awful.layout.suit.fair,
    -- awful.layout.suit.fair.horizontal,
    -- awful.layout.suit.spiral,
    -- awful.layout.suit.spiral.dwindle,
    -- awful.layout.suit.max,
    -- awful.layout.suit.max.fullscreen,
    -- awful.layout.suit.magnifier,
    -- awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
-- }}}



local function set_wallpaper(s)
    -- Wallpaper
    if beautiful.wallpaper then
        local wallpaper = beautiful.wallpaper
        -- If wallpaper is a function, call it with the screen
        if type(wallpaper) == "function" then
            wallpaper = wallpaper(s)
        end
        gears.wallpaper.maximized(wallpaper, s, true)
    end
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)
local mytextclock = widgets.clock()
local mylauncher = widgets.start_menu(globals.get_icons_dir())
local volume_button = widgets.volume_button(globals.get_icons_dir())
local mykeyboardlayout = widgets.clickable(awful.widget.keyboardlayout())

LEFT_SCREEN = nil
MIDDLE_SCREEN = nil
RIGHT_SCREEN = nil

awful.screen.connect_for_each_screen(function(s)
    -- Wallpaper
    set_wallpaper(s)
    if s.workarea.x == 0 then
        LEFT_SCREEN = s
    elseif s.workarea.x == 1280 then
        MIDDLE_SCREEN = s
    else
        RIGHT_SCREEN = s
    end
end)

-- Default to normal screen if there is only one
MIDDLE_SCREEN = nil
if MIDDLE_SCREEN == nil then
    MIDDLE_SCREEN = awful.screen.focused()
    LEFT_SCREEN = MIDDLE_SCREEN
    RIGHT_SCREEN = MIDDLE_SCREEN
end


local sharedtags = require("sharedtags")
TAGS = sharedtags({
    {
        name = "term",
        icon = globals.get_icons_dir() .. "terminal.svg",
        layout = awful.layout.suit.tile,
        screen = RIGHT_SCREEN,
        icon_only = true,
    },
    {
        name = "web",
        icon = globals.get_icons_dir() .. "firefox.svg",
        layout = awful.layout.suit.tile.bottom,
        screen = LEFT_SCREEN,
        selected = true,
        icon_only = true,
        master_count = 2,
    },
    {
        name = "msg",
        icon = globals.get_icons_dir() .. "forum.svg",
        layout = awful.layout.suit.tile,
        screen = RIGHT_SCREEN,
        icon_only = true,
        master_count = 4,
    },
    {
        name = "music",
        icon = globals.get_icons_dir() .. "music.svg",
        layout = awful.layout.suit.tile,
        screen = RIGHT_SCREEN,
        icon_only = true,
        selected = true,
        master_count = 4,
    },
    { layout = awful.layout.suit.tile, screen = MIDDLE_SCREEN },
    { layout = awful.layout.suit.tile, screen = MIDDLE_SCREEN },
    { layout = awful.layout.suit.tile, screen = LEFT_SCREEN },
    {
        name = "code",
        icon = globals.get_icons_dir() .. "code-braces.svg",
        layout = awful.layout.suit.tile,
        screen = MIDDLE_SCREEN,
        selected = true,
        icon_only = true,
    },
    {
        name = "game",
        icon = globals.get_icons_dir() .. "controller.svg",
        layout = awful.layout.suit.tile,
        screen = MIDDLE_SCREEN,
        icon_only = true,
    },
    { layout = awful.layout.suit.tile, screen = LEFT_SCREEN }
})

MIDDLE_SCREEN.mytaglist = widgets.taglist(MIDDLE_SCREEN)
-- This adds an empty bar at the bottom to simulate borders
-- This is a workaround to only have the border on three sides
-- which allows better automatic placement of tooltips/popups/etc
awful.wibar({
    position = "bottom",
    height = beautiful.wibar_border_thickness,
    bg = "#00000000",
    screen = MIDDLE_SCREEN,
})

-- Create the wibox
MIDDLE_SCREEN.mywibox = awful.wibar({
    position = "bottom",
    screen = MIDDLE_SCREEN,
    stretch = false,
    -- Full screen width without border on left and right
    width = MIDDLE_SCREEN.geometry.width - 2 * beautiful.wibar_border_thickness,
})
MIDDLE_SCREEN.systray = wibox.widget.systray()
local systray_hpadding = 20
local systray_vpadding = 7

-- Add widgets to the wibox
MIDDLE_SCREEN.mywibox:setup {
    layout = wibox.layout.flex.horizontal,
    { layout = wibox.layout.fixed.horizontal, mylauncher},
    { layout = wibox.layout.fixed.horizontal, wibox.container.place(MIDDLE_SCREEN.mytaglist) },
    wibox.container.place({ -- Right widgets
        layout = wibox.layout.fixed.horizontal,
        mykeyboardlayout,
        volume_button,
        wibox.container.margin (
            MIDDLE_SCREEN.systray,
            systray_hpadding, --left
            systray_hpadding, --right
            systray_vpadding, --top
            systray_vpadding  --bottom
        ),
        mytextclock,
    }, "right")
}
-- Menu setup

menubar.geometry = {
    x = MIDDLE_SCREEN.mywibox.x + 36,
    y = MIDDLE_SCREEN.mywibox.y,
    width = 756,
    height = 35,
}
menubar.cache_entries = false
menubar.utils.terminal = globals.get_terminal() -- Set the terminal for applications that require it



-- {{{ Key bindings
--local globalkeys = require("config.keys.global")
root.keys(require("config.keys.global"))

require("config.rules")
require("config.signals")
