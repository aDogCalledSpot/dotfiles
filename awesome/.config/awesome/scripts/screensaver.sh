#!/usr/bin/bash

xidlehook \
    --not-when-fullscreen               \
    --not-when-audio                    \
    --timer 180                         \
        ./dim.sh                        \
        ./interrupt_dim.sh              \
    --timer 120                         \
        ./lock.sh                       \
        ''                              \

