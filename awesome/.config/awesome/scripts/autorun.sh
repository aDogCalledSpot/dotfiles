#!/bin/bash

function run {
    if ! pgrep -f $1 ;
    then
        $@&
    fi
}

# Change into scripts directory
cd "$(dirname "$0")"

run picom -b
run setxkbmap gb,us -variant ,dvp -option grp:win_space_toggle -option compose:ralt
run /usr/lib/polkit-kde-authentication-agent-1
run /usr/lib/kdeconnectd
run kdeconnect-indicator
run unclutter -b
run numlockx on
run xset s off
run protonmail-bridge --no-window
run ./screensaver.sh
run ./pwreload.sh
run easyeffects -w 
