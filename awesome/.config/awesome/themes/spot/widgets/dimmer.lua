local awful = require('awful')
local wibox = require('wibox')
local naughty = require('naughty')
local globals = require("config.global_vars")
local gears = require('gears')

-- Get screen geometry
local screen_geometry = awful.screen.focused().geometry


dim_overlay = 
  wibox
  {
    x = screen_geometry.x,
    y = screen_geometry.y,
    height = screen_geometry.height,
    width = screen_geometry.width,
    visible = false,
    ontop = true,
    type = 'splash',
    bg = "#00000000",
  }

--local dim_overlay_grabber
local timer

function dim_overlay_hide(remove_dim)
    --awful.keygrabber.stop(dim_overlay_grabber)
    timer:stop()
    if remove_dim then
        dim_overlay.visible = false
    end
end

local round = function(input)
    return math.floor(input + 0.5)
end

local to_hex_str = function(input)
    return string.format("%x", round(input))
end

--[[
dim_overlay:connect_signal('mouse::move', function()
    dim_overlay_hide(true)
end)
--]]

function dim_overlay_show()
--[[
    dim_overlay_grabber =
        awful.keygrabber.run(
            function(_, key, event)
                if event == 'release' then
                    return
                end
                dim_overlay_hide(true)
            end
        )
--]]
    dim_overlay.visible = true

    local dim_over_seconds = 5
    local dim_update_rate = 0.01
    local total_steps = dim_over_seconds / dim_update_rate
    local dim_threshold = 100
    local alpha_step_size = dim_threshold / total_steps
    
    local alpha_val = 0
    timer = gears.timer.start_new(dim_update_rate,
        function()
            if alpha_val < dim_threshold then
                alpha_val = alpha_val + alpha_step_size
                dim_overlay.bg = "#000000" .. to_hex_str(alpha_val)
            end
            return true
        end
    )

end

