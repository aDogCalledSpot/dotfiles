return {
    clock = require("themes.spot.widgets.clock");
    calendar = require("themes.spot.widgets.calendar");
    start_menu = require("themes.spot.widgets.start_menu");
    volume_button = require("themes.spot.widgets.volume_button");
    taglist = require("themes.spot.widgets.taglist");
    clickable = require("themes.spot.widgets.clickable");
    exit_screen = require("themes.spot.widgets.exit_screen");
    dimmer = require("themes.spot.widgets.dimmer");
}
