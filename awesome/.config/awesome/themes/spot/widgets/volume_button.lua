local wibox = require("wibox")
local awful = require("awful")
local beautiful = require("beautiful")
local clickable = require("themes.spot.widgets.clickable")

local volume = { mt = {} }

local function new(icon_dir)
    slider = wibox.widget.slider()
    slider.forced_width = 80
    slider.forced_height = 35
    slider.minimum = 0
    slider.maximum = 100

    popup = awful.popup {
        widget = {
            layout = wibox.layout.align.vertical,
            {
                widget = wibox.container.background,
                layout = wibox.layout.flex.horizontal,
                {
                    widget = wibox.container.margin,
                    top = 8,
                    bottom = 5,
                    {
                        widget = wibox.container.place,
                        {
                            id = "volume_text",
                            widget = wibox.widget.textbox,
                            text = "100",
                            font = "sans 8"
                        }
                    }
                }
            },
            {
                widget = wibox.container.margin,
                bottom = beautiful.slider_handle_margins.bottom,
                {
                    widget = wibox.container.rotate,
                    {
                        widget = slider,
                    },
                    direction = "east",
                }
            },
            {
                widget = wibox.container.constraint,
                strategy = "max",
                width = 37,
                {
                    widget = clickable,
                    id = "mute_button_bg",
                    {
                        widget = wibox.container.place,
                        layout = wibox.layout.fixed.horizontal,
                        {
                            widget = wibox.container.margin,
                            top = 10,
                            bottom = 5,
                            left = 10,
                            right = 10,
                            {
                                widget = wibox.widget.imagebox,
                                image = icon_dir .. "volume-high.svg",
                                id = "mute-button"
                            }
                        }
                    }
                }
            }
        },
        visible = false,
        ontop = true,
        --hide_on_right_click = true,
        muted = false,
    }
    button = wibox.widget {
        widget = wibox.container.margin,
        top = 8,
        bottom = 8,
        left = 8,
        right = 8,
        {
            widget = wibox.widget.imagebox,
            image = icon_dir .. "volume-very-low.svg",
            id = "icon",
        },
    }
    button:buttons({awful.button({}, 3, function() popup.visible = false end)})
    popup:bind_to_widget(button)

    volume_tooltip = awful.tooltip{ mode = "outside" }

    volume_tooltip:add_to_object(button)

    button:connect_signal("mouse::enter", function()
        if not popup.visible then
            volume_tooltip.visible = true
            if popup.muted then
                volume_tooltip.text = "Muted"
            else
                volume_tooltip.text = "" .. slider.value .. "%"
            end
        else
            volume_tooltip.visible = false
        end
    end)

    -- If not volume use the value in the slider
    local set_volume_image = function(volume)
        if not volume then
            volume = slider.value
        end
        local large = button:get_children_by_id("icon")[1]
        local small = popup.widget:get_children_by_id("mute-button")[1]
        local icon
        if popup.muted then
            icon = icon_dir .. "volume-mute.svg"
        elseif volume <= 5 then
            icon = icon_dir .. "volume-very-low.svg"
        elseif volume <= 35 then
            icon = icon_dir .. "volume-low.svg"
        elseif volume <= 70 then
            icon = icon_dir .. "volume-medium.svg"
        else
            icon = icon_dir .. "volume-high.svg"
        end
        large.image = icon
        small.image = icon
    end

    local set_volume_ui = function(volume)
        local set_volume_text = function()
            popup.widget:get_children_by_id("volume_text")[1].text = "" .. volume -- Removes \n
            slider:set_value(volume)
        end

        set_volume_text()
        set_volume_image(volume)
    end

    slider:connect_signal("property::value", function()
        local volume = slider.value
        awful.spawn("pamixer --set-volume " .. volume)
        set_volume_ui(volume)
        end)


    awesome.connect_signal("volume::volume", function()
        awful.spawn.easy_async_with_shell("pamixer --get-volume", function(stdout)
            set_volume_ui(tonumber(stdout))
        end)
    end)

    awesome.connect_signal("volume::mute", function()
        awful.spawn.easy_async_with_shell("pamixer --get-mute", function(stdout)
            popup.muted = stdout == "true\n"
            set_volume_image(nil)
        end)
    end)

    popup.widget:get_children_by_id("mute_button_bg")[1]:buttons({awful.button({}, 1, nil, function()
        popup.muted = not popup.muted
        set_volume_image(nil)
        awful.spawn("pamixer --toggle-mute")
    end)})

    awful.widget.watch('bash -c "pamixer --get-volume"', 2,
            function(_, stdout)
                local volume = tonumber(stdout)
                popup.widget:get_children_by_id("volume_text")[1].text = "" .. volume -- Removes \n
                slider:set_value(volume)
                collectgarbage("collect")
            end)

    awful.widget.watch('bash  -c "pamixer --get-mute"', 2,
            function(_, stdout)
                popup.muted = stdout == "true\n"
                set_volume_image(nil)
            end)

    return clickable(button)
end


function volume.mt:__call(...)
    return new(...)
end

return setmetatable(volume, volume.mt)