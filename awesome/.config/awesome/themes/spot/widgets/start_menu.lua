-- Create a launcher widget and a main menu
local awful = require("awful")
local wibox = require("wibox")
local menubar = require("menubar")

local launcher = { mt = {} }



local function new(icon_dir)
    local submenu = {
        { "hotkeys", function() hotkeys_popup.show_help(nil, awful.screen.focused()) end },
        { "restart", awesome.restart },
        { "quit", function() awesome.quit() end },
    }

    local main_menu = awful.menu({ items = { { "awesome", submenu } } })

    local launcher_menu_padding = 4
    local launcher_menu_padding_loffset = 1
    local tux_bg_color = "#67a2a5"
    local tux_border_color = "#75aabb"

    local launcher = wibox.container.background(
                    wibox.container.margin(
                        wibox.container.margin(
                            wibox.widget.imagebox(icon_dir .. "tux.svg"),
                            launcher_menu_padding +  launcher_menu_padding_loffset, --left
                            launcher_menu_padding, --right
                            launcher_menu_padding, --top
                            launcher_menu_padding --bottom
                            ),
                        1, 1, 1, 1, tux_border_color),
                    tux_bg_color)
    launcher:buttons({
                awful.button({}, 1, nil, function() menubar.show() end),
                awful.button({}, 2, nil, function() main_menu:toggle() end),
                awful.button({}, 3, nil, function() exit_screen_show() end)}
    )
    return launcher
end

function launcher.mt:__call(...)
    return new(...)
end

return setmetatable(launcher, launcher.mt)