local wibox = require("wibox")
local awful = require("awful")
local cal = require("themes.spot.widgets.calendar")
local clickable = require("themes.spot.widgets.clickable")

local clock = { mt = {} }

local function new()
    local widget = wibox.widget {
        widget = wibox.container.margin,
        right = 10,
        left = 10,
        {
            widget = wibox.widget.textclock("<b><big>%H:%M</big></b>"),
        },
    }

    local calendar = cal()
    calendar:bind_to_widget(widget)

    local clock_tooltip = awful.tooltip{ mode = "outside", }
    clock_tooltip:add_to_object(widget)

    widget:connect_signal("mouse::enter", function()
        if not calendar.visible then
            clock_tooltip.visible = true
            clock_tooltip.text = os.date("%A, %B %d %Y")
        else
            clock_tooltip.visible = false
        end
    end)

    return clickable(widget)
end

function clock.mt:__call(...)
    return new(...)
end

return setmetatable(clock, clock.mt)