local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")

local taglist = { mt = {} }

local function new(scr)
    local tag_width = 75
    local icon_vpadding = 7

    local taglist_buttons = {awful.button({ }, 1, function(t) t:view_only() end)}

    local taglist = awful.widget.taglist {
            screen  = scr,
            filter  = awful.widget.taglist.filter.all,
            source  = function() return root.tags() end,
            layout  = wibox.layout.flex.horizontal,
            widget_template = {
                -- display the background
                id = 'background_role',
                widget = wibox.container.background,
                -- ensure all tags have the same width
                forced_width = tag_width,
                {
                    -- center the text/icons
                    widget = wibox.container.place,
                    {
                        -- this line is needed
                        layout = wibox.layout.fixed.horizontal,
                        {
                            -- vertical padding on icons
                            widget  = wibox.container.margin,
                            top = icon_vpadding,
                            bottom = icon_vpadding,
                            -- display icons
                            {
                                id     = 'icon_role',
                                widget = wibox.widget.imagebox,
                            },
                        },
                        -- display text
                        {
                            id     = 'text_role',
                            widget = wibox.widget.textbox,
                        },
                    },
                },
                -- Add support for hover colors
                create_callback = function(self, c3, index, objects) --luacheck: no unused args
                    local is_color = function(varColor, compColor)
                        if not varColor then return false end
                        local _, r_color, g_color, b_color, a_color = varColor:get_rgba()
                        r_color = r_color * 255
                        g_color = g_color * 255
                        b_color = b_color * 255
                        a_color = a_color * 255

                        local r_comp = tonumber(string.sub(compColor, 2, 3), 16)
                        local g_comp = tonumber(string.sub(compColor, 4, 5), 16)
                        local b_comp = tonumber(string.sub(compColor, 6, 7), 16)
                        local a_comp
                        if #compColor == 7 then
                            a_comp = 255
                        else
                            a_comp = tonumber(string.sub(compColor, 8, 9), 16)
                        end

                        if math.abs(r_comp - r_color) > 2 then return false end
                        if math.abs(g_comp - g_color) > 2 then return false end
                        if math.abs(b_comp - b_color) > 2 then return false end

                        return true
                    end

                    local is_focused = function(color) return is_color(color, beautiful.taglist_bg_focus) end

                    local is_urgent = function(color) return is_color(color, beautiful.taglist_bg_urgent) end

                    self:connect_signal('mouse::enter', function()
                        self.backup     = self.bg
                        self.has_backup = true
                        -- is_focused will assure it is not being hovered
                        if type(self.bg) ~= "string" then
                            if is_focused(self.bg) then
                                self.bg = beautiful.taglist_hover_focus
                            elseif is_urgent(self.bg) then
                                self.bg = beautiful.taglist_hover_urgent
                            else
                                self.bg = beautiful.taglist_hover_normal
                            end
                        elseif self.bg ~= beautiful.taglist_hover_normal then
                            self.bg = beautiful.taglist_hover_normal
                        end
                    end)
                    self:connect_signal('mouse::leave', function()
                        if self.has_backup then
                            self.bg = self.backup
                        end
                    end)
                end,
                update_callback = function(self, c3, index, objects) --luacheck: no unused args
                    self.has_backup = true
                    self.backup = beautiful.bg_focus
                    self.bg = beautiful.taglist_hover_focus
                end,
            },
            -- enable clicking
            buttons = taglist_buttons
        }
    return taglist
end

function taglist.mt:__call(...)
    return new(...)
end

return setmetatable(taglist, taglist.mt)
