local wibox = require("wibox")
local awful = require("awful")
local gears = require("gears")

local calendar = { mt = {} }

local function new()
    local popup = awful.popup{
        widget = wibox.widget
        {
            widget = wibox.container.margin,
            top = 3,
            left = 7,
            right = 7,
            {
                widget = wibox.widget.calendar.month,
                date = os.date('*t'),
                font = 'sans 10',
                id = "calendar"
            },
        },
        ontop = true,
        visible = false,
        hide_on_right_click = true,
    }

    gears.timer {
        timeout = 60,
        call_now = true,
        autostart = true,
        callback = function()
            popup.widget:get_children_by_id("calendar")[1].date = os.date("*t")
        end
    }

    return popup
end

function calendar.mt:__call(...)
    return new(...)
end

return setmetatable(calendar, calendar.mt)