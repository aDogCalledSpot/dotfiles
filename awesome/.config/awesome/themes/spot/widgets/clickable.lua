local wibox = require('wibox')

local clickable = {mt = {}}

local function new(widget)
    local container = wibox.container.background(widget)

    container:connect_signal(
        'mouse::enter', function()
            container.bg = '#ffffff11'
        end
    )

    container:connect_signal(
        'mouse::leave', function()
            container.bg = '#ffffff00'
        end
    )

    container:connect_signal(
        'button::press', function()
            container.bg = '#ffffff22'
        end
    )

    container:connect_signal(
        'button::release', function()
            container.bg = '#ffffff11'
        end
    )

    return container
end

function clickable.mt:__call(...)
    return new(...)
end

return setmetatable(clickable, clickable.mt)