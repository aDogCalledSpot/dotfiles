-- Menu setup
local menubar = require("menubar")

menubar.show_categories = false
menubar.match_empty  = false
menubar.prompt_args = {
    --[[
        This should technically be fetched from beautiful
        but since this file is included by theme.lua it's
        not possible to access the values so they are hard-
        coded here
    --]]
    prompt = "     ",
    bg_cursor = "#5b7a95"
}