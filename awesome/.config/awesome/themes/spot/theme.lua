local theme = {}

--require("themes.spot.widgets.clock")
-- User settings
theme.wallpaper = "/home/ari/pictures/sekiro.jpg"

-- General colours
local transparency = "da"
local bg = "#295174"
local focus = "#398c92"
local urgent = "#6cc474"
local white = "#ffffff"
local highlight = "#3f7db3"
local bg_transparent = bg .. transparency
local dark = "#162c3f"
local dark_transparent = dark .. transparency
local red = "#ff232d"

-- General
theme.font = "sans 13"
theme.border_normal = "#00000000"
theme.bg_normal = bg
theme.bg_urgent = urgent
theme.bg_focus = focus
theme.border_focus = focus
theme.border_width = 2
theme.useless_gap = 4

-- Taglist
theme.taglist_hover_normal = "#3f7db3"
theme.taglist_hover_focus = "#23969c"
theme.taglist_hover_urgent = "#59cb79"
theme.taglist_squares_sel = nil
theme.taglist_squares_unsel = nil
theme.taglist_bg_focus = focus
theme.taglist_bg_empty = bg
theme.taglist_bg_occupied = bg
theme.taglist_bg_urgent = urgent

-- Wibar
theme.wibar_bg = bg

-- This isn't wibar_border_width!!
-- This is a workaround to only have the border on three sides
-- which allows better automatic placement of tooltips/popups/etc
theme.wibar_border_thickness = 7
theme.wibar_height = 35

-- Systray
theme.bg_systray = bg
theme.systray_icon_spacing = 3

-- Hotkeys
theme.hotkeys_bg = dark_transparent
theme.hotkeys_label_bg = dark_transparent
theme.hotkeys_modifiers_fg = white
theme.hotkeys_label_fg = white

-- Menu
local menu_bg = "#5b7a95"
theme.menu_bg_normal = menu_bg
theme.menu_bg_focus = white .. "1a"
theme.menu_fg_normal = white
theme.menu_fg_focus = white
theme.prompt_bg = menu_bg

-- Slider
theme.slider_bar_color = dark
theme.slider_handle_color = focus
theme.slider_bar_height = 1
theme.slider_handle_width = 5
theme.slider_handle_margins = {
    top = 7,
    bottom = 7
}

-- Tooltip
theme.tooltip_bg = bg_transparent
theme.tooltip_fg = white

-- Notifications
local notification_height = 100
theme.notification_width = 300
theme.notification_icon_size = notification_height
theme.notification_height = notification_height + 10
theme.notification_bg = bg_transparent

require("themes.spot.defaults.menubar")

return theme
