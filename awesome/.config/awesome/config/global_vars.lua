local fs = require("gears.filesystem")
-- {{{ Variable definitions
-- This is used later as the default terminal and editor to run.

local _M = {}


function _M.get_terminal()
    return "alacritty"
end

function _M.get_editor()
    return "vim"
end

function _M.get_browser()
    return "firefox-nightly"
end

function _M.get_editor_cmd()
    return _M.get_terminal() .. " -e " .. _M.get_editor()
end

function _M.get_modkey()
    return "Mod4"
end

function _M.get_theme()
    return "spot"
end

function _M.get_theme_dir()
    return fs.get_configuration_dir() .. "themes/" .. _M.get_theme() .. "/"
end

function _M.get_icons_dir()
    return _M.get_theme_dir() .. "icons/"
end

function _M.get_lockscreen()
    return "betterlockscreen -l --time-format \"%H:%M\" --off 60 --show-layout"
end

return _M
