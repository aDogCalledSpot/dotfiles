local gears = require("gears")
local awful = require("awful")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")
local globals = require("config.global_vars")

local keys = gears.table.join(
    awful.key({ globals.get_modkey(),           }, "g",      hotkeys_popup.show_help,
              {description="show help", group="launcher"}),
    awful.key({ globals.get_modkey(),           }, "BackSpace", awful.tag.history.restore,
              {description = "go back", group = "tag"}),

    -- media keys
    awful.key({                   }, "XF86AudioPlay",
        function() awful.spawn("playerctl play-pause") end),
    awful.key({                   }, "XF86AudioNext",
        function() awful.spawn("playerctl next") end),
    awful.key({                   }, "XF86AudioPrev",
        function() awful.spawn("playerctl previous") end),
    awful.key({                      }, "XF86AudioRaiseVolume",
        function()
            awesome.emit_signal("volume::volume")
            awful.spawn("pactl set-sink-volume @DEFAULT_SINK@ +5%")
        end),
    awful.key({                      }, "XF86AudioLowerVolume",
        function()
            awesome.emit_signal("volume::volume")
            awful.spawn("pactl set-sink-volume @DEFAULT_SINK@ -5%")
        end),
    awful.key({                      }, "XF86AudioMute",
        function()
            awesome.emit_signal("volume::mute")
            awful.spawn("pactl set-sink-mute  @DEFAULT_SINK@ toggle")
        end),
    awful.key({ globals.get_modkey(),           }, "j",
        function ()
            awful.client.focus.byidx( 1)
        end,
        {description = "focus next by index", group = "client"}
    ),
    awful.key({ globals.get_modkey(),           }, "k",
        function ()
            awful.client.focus.byidx(-1)
        end,
        {description = "focus previous by index", group = "client"}
    ),

    -- Layout manipulation
    awful.key({ globals.get_modkey(), "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
              {description = "swap with next client by index", group = "client"}),
    awful.key({ globals.get_modkey(), "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
              {description = "swap with previous client by index", group = "client"}),
    awful.key({ globals.get_modkey(), "Control" }, "k", function () awful.client.movetoscreen() end,
              {description = "Move client to next screen", group = "screen"}),

    -- Standard program
    awful.key({ globals.get_modkey(),           }, "Return", function () awful.spawn(globals.get_terminal())              end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ globals.get_modkey(),           }, ";", function () awful.spawn(globals.get_terminal())           end,
              {description = "open a terminal", group = "launcher"}),
    awful.key({ globals.get_modkey(),           }, "b", function() awful.spawn(globals.get_browser())                    end,
              {description = "open a browser", group = "launcher"}),
    awful.key({ globals.get_modkey(), "Shift"   }, "f", awesome.restart,
              {description = "reload awesome", group = "awesome"}),
    awful.key({                   }, "Print", function() awful.spawn("flameshot gui") end,
              {description = "take a screenshot", group = "launcher"}),
    awful.key({ globals.get_modkey(), "Shift"   }, "d", function() exit_screen_show() end,
              {description = "quit awesome", group = "awesome"}),

    awful.key({ globals.get_modkey(),           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
              {description = "increase master width factor", group = "layout"}),
    awful.key({ globals.get_modkey(),           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
              {description = "decrease master width factor", group = "layout"}),
    awful.key({ globals.get_modkey(), "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
              {description = "increase the number of master clients", group = "layout"}),
    awful.key({ globals.get_modkey(), "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
              {description = "decrease the number of master clients", group = "layout"}),
    awful.key({ globals.get_modkey(), "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
              {description = "increase the number of columns", group = "layout"}),
    awful.key({ globals.get_modkey(), "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
              {description = "decrease the number of columns", group = "layout"}),
    awful.key({ globals.get_modkey(), "Control" }, "n",
              function ()
                  local c = awful.client.restore()
                  -- Focus restored client
                  if c then
                    c:emit_signal(
                        "request::activate", "key.unminimize", {raise = true}
                    )
                  end
              end,
              {description = "restore minimized", group = "client"}),

    -- Menubar
    awful.key({ globals.get_modkey() }, "a", menubar.show,
             {description = "launch an application", group = "launcher"})
)

local tag_keys = { "q", "w", "e", "r", "t", "y", "u", "i", "o", "p" }

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
local sharedtags = require("sharedtags")
for i = 1, 10 do
    local key = tag_keys[i]
    keys = gears.table.join(keys,
        -- View tag only.
        awful.key({ globals.get_modkey() }, key,
                  function ()
                        local tag = TAGS[i]
                        if tag then
                            sharedtags.viewonly(tag, tag.screen)
                        end
                        awful.screen.focus(tag.screen)
                  end,
                  {description = "view tag #"..i, group = "tag"}),
        -- Move client to tag.
        awful.key({ globals.get_modkey(), "Shift" }, key,
                  function ()
                      if client.focus then
                          local tag = TAGS[i]
                          if tag then
                              client.focus:move_to_tag(tag)
                          end
                     end
                  end,
                  {description = "move focused client to tag #".. i, group = "tag"})
    )
end

for i = 1, screen.count() do
    local key = tag_keys[i]
    keys = gears.table.join(keys,
        awful.key({ globals.get_modkey(), "Control" }, key,
                  function ()
                      local tag = awful.screen.focused().selected_tags[1]
                      sharedtags.movetag(tag, i)
                      sharedtags.viewonly(tag, i)
                      awful.screen.focus(i)
                  end,
                  {description = "move current tag to screen #".. i, group = "screen"})
    )
end

return keys
