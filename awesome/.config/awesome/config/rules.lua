local awful = require("awful")
local beautiful = require("beautiful")

local client_keys = require("config.keys.client")
local client_buttons = require("config.buttons.client")

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = {
    -- All clients will match this rule.
    { rule = { },
        properties = { border_width = beautiful.border_width,
                    border_color = beautiful.border_normal,
                    focus = awful.client.focus.filter,
                    raise = true,
                    keys = client_keys,
                    buttons = client_buttons,
                    screen = awful.screen.preferred,
                    placement = awful.placement.no_overlap+awful.placement.no_offscreen
        }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "pinentry",
        },
        class = {
          "Arandr",
          "Blueman-manager",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
          "Wpa_gui",
          "veromix",
          "xtightvncviewer",
          "dolphin",
          "Steam - News",
          "steam_app_383980" -- Rivals Of Aether
        },

        -- Note that the name property shown in xprop might be set slightly after creation of the client
        -- and the name shown there might not match defined rules here.
        name = {
          "Event Tester",  -- xev.
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "ConfigManager",  -- Thunderbird's about:config.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = {
            floating = true,
            placement = awful.placement.centered + awful.placement.no_overlap + awful.placement.no_offscreen}},

    { rule = { class = "Steam", name = "Friends List" }, properties = { floating = true}},
    { rule = { class = "Steam", name = "Steam - News" }, properties = { floating = true}},

    -- Add titlebars to normal clients and dialogs
    -- { rule_any = {type = { "normal", "dialog" }
     -- }, properties = { titlebars_enabled = false }
    --},

    { rule_any = { class = {"Telegram", "discord", "Signal", "whatsapp", "Element"}},
        properties = { tag = TAGS["msg"] } },

    { rule_any = { class = {"Tauon Music Box", "Spotify", "tidal-hifi"}}, properties = { tag = TAGS["music"] } },

    { rule_any = { class = {"jetbrains", "code-oss", "Godot"}}, properties = { tag = TAGS["code"] } },

    { rule_any = { class = {"Steam"}}, properties = { tag = TAGS["game"] } },
}
-- }}}
