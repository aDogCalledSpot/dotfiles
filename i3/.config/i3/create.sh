#!/bin/bash

cd "$(dirname "$0")"

cat startup style shared_keys > main

cat main keys_qwerty append > config-qwerty
cat main keys_dvorak append > config-dvorak

rm main
