#!/bin/bash

cd "$(dirname "$0")"

# List all explicitly installed packages
yay -Qeq > yay.txt
flatpak list --columns=application > flatpak.txt

git add -u
git commit -m \""$(date +%d.%m.%Y)"\"
git push
